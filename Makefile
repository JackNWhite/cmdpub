CC=gcc
CFLAGS=`pkg-config --cflags gtk+-3.0` -DDEBUG -g3
LIBS=`pkg-config --libs gtk+-3.0`

cmdpub: cmdpub.o
	$(CC) -o $@ cmdpub.o $(LIBS)

cmdpub.o: cmdpub.c
	$(CC) -c $(CFLAGS) cmdpub.c