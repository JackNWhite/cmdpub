/* A programme to send position commands to GPS/ROS controllers */

#include <gtk/gtk.h>
#include <stdint.h>
#include <stdlib.h>
#include <string.h>

// Macros
#define CMD_LEN 500

// Data types
struct raw_pos_data_t
{
  GtkEntryBuffer* pos[7];
  GtkEntryBuffer* pid[4 * 7];
} raw_pos_data;


// Variable declarations
uint32_t msg_id = 0;
const gchar* pos_cmd_note_txt =
  "Position commands are in the format <tt>id: u32, mode: 1, arm: 0, data[7]: f64, pid:[4*7]: f64</tt>\nThe fourth PID characteristic is, as yet, unknown.";

const gchar* pos_cmd_preamble =
  "rostopic pub -1 /gps_controller_position_command gps_agent_pkg/PositionCommand \"{";
char default_pids[][4 * 7] = {"2400.0", "0.0", "18.0", "4.0",
                              "1200.0", "0.0", "20.0", "4.0",
                              "1000.0", "0.0", "6.0", "4.0",
                              "700.0", "0.0", "4.0", "4.0",
                              "300.0", "0.0", "6.0", "2.0",
                              "300.0", "0.0", "4.0", "2.0",
                              "300.0", "0.0", "2.0", "2.0"};


// Function declarations
static void activate(GtkApplication *app, gpointer user_data);
static gboolean on_key_press(GtkWidget *wnd, GdkEventKey *event, gpointer data);
static void send_pos_cmd(GtkWidget* widget, gpointer *data);


// Definitions
int
main(int argc, char** argv)
{
  GtkApplication* app;
  int status;

  app = gtk_application_new("jackwhite.gps.cmdpub", G_APPLICATION_FLAGS_NONE);
  g_signal_connect(app, "activate", G_CALLBACK(activate), NULL);
  status = g_application_run(G_APPLICATION(app), argc, argv);
  g_object_unref(app);

  return status;
}


static void
activate(GtkApplication *app, gpointer user_data)
{
  /* Top-level widgets */
	GtkWidget* window;
  GtkWidget* opera_box; /* Vertical packing box */

  /* Position commanding widgets */
  GtkWidget* pos_cmd_frame; /* Caters for later trial command implementation */
  GtkWidget* pos_cmd_box; /* Caters for later trial command implementation */
  GtkWidget* pos_cmd_note; /* Label with notes on position commands */
  GtkWidget* pos_cmd_grid; /* Organises position widgets */
  GtkWidget* pos_label; /* Reads 'Position' */
  GtkWidget* pid_label; /* Reads 'PID' */
  GtkWidget* pos_boxes[7]; /* Positions to command */
  GtkWidget* pid_boxes[4 * 7]; /* PID gains + mystery component */
  GtkWidget* pos_cmd_btn; /* Command button */

  window = gtk_application_window_new(app);
  gtk_window_set_title(GTK_WINDOW (window), "GPS Command Publisher");
  gtk_window_set_default_size(GTK_WINDOW (window), 0, 0);
  gtk_container_set_border_width (GTK_CONTAINER (window), 10);
  g_signal_connect(G_OBJECT(window), "key_press_event",
                            G_CALLBACK(on_key_press), NULL);

  opera_box = gtk_box_new(GTK_ORIENTATION_VERTICAL, 3);
  gtk_container_add(GTK_CONTAINER(window), opera_box);

  pos_cmd_frame = gtk_frame_new("Position command");
  gtk_box_pack_start(GTK_BOX(opera_box), pos_cmd_frame,
                     TRUE, TRUE, 0);

  pos_cmd_box = gtk_box_new(GTK_ORIENTATION_VERTICAL, 3);
  gtk_container_set_border_width (GTK_CONTAINER (pos_cmd_box), 10);
  gtk_container_add(GTK_CONTAINER(pos_cmd_frame), pos_cmd_box);

  pos_cmd_note = gtk_label_new(NULL);
  gtk_label_set_markup(GTK_LABEL(pos_cmd_note), pos_cmd_note_txt);
  gtk_box_pack_start(GTK_BOX(pos_cmd_box), pos_cmd_note, TRUE, TRUE, 0);

  pos_cmd_grid = gtk_grid_new();
  pos_label = gtk_label_new("Joint positions");
  pid_label = gtk_label_new("PID gains + mystery");
  gtk_grid_attach(GTK_GRID(pos_cmd_grid), pos_label, 0, 0, 1, 1);
  gtk_grid_attach(GTK_GRID(pos_cmd_grid), pid_label, 0, 1, 1, 1);

  for (int i = 0; i < 7; i++)
    {
      pos_boxes[i] = gtk_entry_new();
      gtk_grid_attach(GTK_GRID(pos_cmd_grid), pos_boxes[i], 1 + i * 4, 0, 4, 1);
      raw_pos_data.pos[i] = gtk_entry_get_buffer(GTK_ENTRY(pos_boxes[i]));
      for (int j = 0; j < 4; j++)
        {
          pid_boxes[i * 4 + j] = gtk_entry_new();
          gtk_entry_set_width_chars(GTK_ENTRY(pid_boxes[i * 4 + j]), 5);
          gtk_grid_attach(GTK_GRID(pos_cmd_grid), pid_boxes[i * 4 + j],
                          1 + i * 4 + j, 1, 1, 1);
          raw_pos_data.pid[i * 4 + j] = 
            gtk_entry_get_buffer(GTK_ENTRY(pid_boxes[i * 4 + j]));
        }
    }
  gtk_box_pack_start(GTK_BOX(pos_cmd_box), pos_cmd_grid, TRUE, TRUE, 0);

  pos_cmd_btn = gtk_button_new_with_label("Go");
  gtk_box_pack_start(GTK_BOX(pos_cmd_box), pos_cmd_btn, TRUE, TRUE, 10);
  g_signal_connect(G_OBJECT(pos_cmd_btn), "clicked",
                   G_CALLBACK(send_pos_cmd),NULL);

  // Fill defaults
  for (int i = 0; i < 7; i++)
    gtk_entry_set_text(pos_boxes[i], "0.0");
  
  for (int i = 0; i < 4 * 7; i++)
    gtk_entry_set_text(pid_boxes[i], default_pids[i]);

  gtk_widget_show_all(window);
}


static gboolean
on_key_press(GtkWidget *wnd, GdkEventKey *event, gpointer data)
{
  // Adds Ctrl+w exit
  if (event->keyval == GDK_KEY_w && event->state & GDK_CONTROL_MASK)
  {
    gtk_window_close(GTK_WINDOW(wnd));
  }
  
  return FALSE;
}


static void
send_pos_cmd(GtkWidget* widget, gpointer *data)
{
  char temp_str[20];

  gchar cmd_buf[CMD_LEN];
  cmd_buf[0] = '\0';

  g_strlcat(cmd_buf, pos_cmd_preamble, CMD_LEN); /* Set up command */
  /* Add id */
  g_strlcat(cmd_buf, "id: ", CMD_LEN);
  sprintf(temp_str, "%d", msg_id++);
  g_strlcat(cmd_buf, temp_str, CMD_LEN);
  strcpy(temp_str, "");

  /* Mode 1 for joint space, use only available arm, start position vector */
  g_strlcat(cmd_buf, ", mode: 1, arm: 0, data: [", CMD_LEN); 

  /* Insert positions */
  for (int i = 0; i < 7; i++)
    {
      g_strlcat(cmd_buf, gtk_entry_buffer_get_text(raw_pos_data.pos[i]),
                CMD_LEN);
      if (i == 6)
        g_strlcat(cmd_buf, "], pd_gains: [", CMD_LEN);
      else
        g_strlcat(cmd_buf, ", ", CMD_LEN);
    }

  /* Insert pid gains */
  for (int i = 0; i < 7; i++)
    {
      for (int j = 0; j < 4; j++)
        {
          g_strlcat(cmd_buf,
                    gtk_entry_buffer_get_text(raw_pos_data.pid[4 * i + j]),
                    CMD_LEN);
          if (i == 6 && j == 3)
            g_strlcat(cmd_buf, "]}\"", CMD_LEN);
          else
            g_strlcat(cmd_buf, ", ", CMD_LEN);
        }
    }
  
  g_print("%s\n", cmd_buf);
  system(cmd_buf);
}